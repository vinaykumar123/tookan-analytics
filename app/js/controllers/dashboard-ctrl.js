App.controller('RevenueController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state, responseCode,$timeout) {
    $scope.loading=true;
    $scope.daily=true;
    $scope.week=false;
    $scope.month=false;
    $scope.dailyavg=true;
    $scope.weekavg=false;
    $scope.monthavg=false;
    
    $scope.getdaily=function(){
        $scope.daily=true;
        $scope.week=false;
        $scope.month=false;
    };
    $scope.getdailyavg=function(){
         $scope.dailyavg=true;
         $scope.weekavg=false;
         $scope.monthavg=false;
    };
    $scope.getweekly=function(){
        $scope.loading1 = true;
        $scope.daily=false;
        $scope.week=true;
        $scope.month=false;
        
        //    ======================weekly revenue=====================
        $http.get('http://23.236.54.158:3000/revenue/weekly').
        success(function(response){
            console.log(response.data.length);
            var xAxis = {
                type: 'datetime'
            };
            var series =  [{
                name: 'Weekly Revenue',
                data: []
            }];
            
            var c=[];
            for(var m=0;m<response.data.length;m++){
                c[m]=new Date(response.data[m].date);
                series[0].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data[m].amount]);
            }
            var title = {
                text: 'Weekly Revenue'   
            };
            var subtitle = {
                text: 'Source: Base_Address/revenue/weekly'
            };
            var yAxis = {
                title: {
                    text: 'Amount (in $)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]};   
            var colors=['#004D75'];
            var tooltip = {
                valueSuffix: '$'
            };

//        var legend = {
//            layout: 'vertical',
//            align: 'right',
//            verticalAlign: 'middle',
//            borderWidth: 0
//        };
            var json = {};
            json.title = title;
            json.subtitle = subtitle;
            json.xAxis = xAxis;
            json.yAxis = yAxis;
            json.colors=colors;
            json.tooltip = tooltip;
            //   json.legend = legend;
            json.series = series;
            $timeout(function(){
                $scope.loading=false;
                $scope.loading1=false;
                $(document).ready(function() {
                    $('#container1').highcharts(json);
                });
            },2000);
        });
    };
    
     $scope.getmonthly=function(){
        $scope.loading1 = true;
        $scope.daily=false;
        $scope.week=false;
        $scope.month=true;
         //    ===========================monthly revenue===================
         $http.get('http://23.236.54.158:3000/revenue/monthly').
         success(function(response){
             console.log(response.data.length);
             var xAxis = {
                 type: 'datetime'
             };
             var series =  [{
                 name: 'Monthly Revenue',
                 data: []
             }];
             var c=[];
             for(var m=0;m<response.data.length;m++){
                 c[m]=new Date(response.data[m].date);
                 series[0].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data[m].amount]);
             }
             var title = {
                 text: 'Monthly Revenue'   
             };
             var subtitle = {
                 text: 'Source: Base_Address/revenue/monthly'
             };
             var yAxis = {
                 title: {
                     text: 'Amount (in $)'
                 },
                 plotLines: [{
                     value: 0,
                     width: 1,
                     color: '#808080'
                 }]};   
             var colors=['#004D75'];
             var tooltip = {
                 valueSuffix: '$'
             };
             //        var legend = {
             //            layout: 'vertical',
             //            align: 'right',
             //            verticalAlign: 'middle',
             //            borderWidth: 0
             //        };
             
             var json = {};
             json.title = title;
             json.subtitle = subtitle;
             json.xAxis = xAxis;
             json.yAxis = yAxis;
             json.colors=colors;
             json.tooltip = tooltip;
             //   json.legend = legend;
             json.series = series;
             $timeout(function(){
                 $scope.loading=false;
                 $scope.loading1=false;
                 $(document).ready(function() {
                     $('#container2').highcharts(json);
                 });
             },2000);
         });
     };
                 
                 $http.get('http://23.236.54.158:3000/revenue/daily').
         success(function(response){
             console.log(response.data.length);
             var xAxis = {
                 type: 'datetime'
             };
             var series =  [
                 {
                     name: 'Daily Revenue',
                     data: []
                 }
             ];
             var c=[];
             for(var m=0;m<response.data.length;m++){
                 c[m]=new Date(response.data[m].date);
                 series[0].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data[m].amount]);
             }
             //        $scope.loading=false;
             var title = {
                 text: 'Daily Revenue'   
             };
             var subtitle = {
                 text: 'Source: Base_Address/revenue/daily'
             };        
             var yAxis = {
                 title: {
                     text: 'Amount (in $)'
                 },
                 plotLines: [{
                     value: 0,
                     width: 1,                              
                     color: '#808080'
                 }]
             };   
             var colors=['#004D75'];
             var tooltip = {
                 valueSuffix: '$'
             }

//        var legend = {
//            layout: 'vertical',
//            align: 'right',
//            verticalAlign: 'middle',
//            borderWidth: 0
//        };

             var json = {};
             json.title = title;
             json.subtitle = subtitle;
             json.xAxis = xAxis;
             json.yAxis = yAxis;
             json.colors=colors;
             json.tooltip = tooltip;
             //   json.legend = legend;
             json.series = series;
             $timeout(function(){
                 $scope.loading=false;
                 $scope.loading1=false;
                 $(document).ready(function() {
                     $('#container').highcharts(json);
                 });
             },2000);
         });
    
    $scope.getweeklyavg=function(){
        $scope.loading2 = true;
        $scope.dailyavg=false;
        $scope.weekavg=true;
        $scope.monthavg=false;
        
        //    ======================weekly avg revenue=====================
        $http.get('http://23.236.54.158:3000/revenue/avg/weekly').
        success(function(response){
            console.log(response.data.length);
            var xAxis = {
                type: 'datetime'
            };
            var series =  [{
                name: 'Average',
                data: []
            }];
            
            var c=[];
            for(var m=0;m<response.data.length;m++){
                c[m]=new Date(response.data[m].date);
                series[0].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data[m].average]);
            }
            var title = {
                text: 'Average Weekly Revenue'   
            };
            var subtitle = {
                text: 'Source: Base_Address/revenue/avg/weekly'
            };
            var yAxis = {
                title: {
                    text: 'Amount (in $)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]};   
            var colors=['#004D75'];
            var tooltip = {
                valueSuffix: '$'
            };

//        var legend = {
//            layout: 'vertical',
//            align: 'right',
//            verticalAlign: 'middle',
//            borderWidth: 0
//        };
            var json = {};
            json.title = title;
            json.subtitle = subtitle;
            json.xAxis = xAxis;
            json.yAxis = yAxis;
            json.colors=colors;
            json.tooltip = tooltip;
            //   json.legend = legend;
            json.series = series;
            $timeout(function(){
                $scope.loading=false;
                $scope.loading2=false;
                $(document).ready(function() {
                    $('#container4').highcharts(json);
                });
            },2000);
        });
    };
    
     $scope.getmonthlyavg=function(){
        $scope.loading2 = true;
        $scope.dailyavg=false;
        $scope.weekavg=false;
        $scope.monthavg=true;
         //    ===========================monthly avg revenue===================
         $http.get('http://23.236.54.158:3000/revenue/avg/monthly').
         success(function(response){
             console.log(response.data.length);
             var xAxis = {
                 type: 'datetime'
             };
             var series =  [{
                 name: 'Average',
                 data: []
             }];
             var c=[];
             for(var m=0;m<response.data.length;m++){
                 c[m]=new Date(response.data[m].date);
                 series[0].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data[m].average]);
             }
             var title = {
                 text: 'Average Monthly Revenue'   
             };
             var subtitle = {
                 text: 'Source: Base_Address/revenue/avg/monthly'
             };
             var yAxis = {
                 title: {
                     text: 'Amount (in $)'
                 },
                 plotLines: [{
                     value: 0,
                     width: 1,
                     color: '#808080'
                 }]};   
             var colors=['#004D75'];
             var tooltip = {
                 valueSuffix: '$'
             };
             //        var legend = {
             //            layout: 'vertical',
             //            align: 'right',
             //            verticalAlign: 'middle',
             //            borderWidth: 0
             //        };
             
             var json = {};
             json.title = title;
             json.subtitle = subtitle;
             json.xAxis = xAxis;
             json.yAxis = yAxis;
             json.colors=colors;
             json.tooltip = tooltip;
             //   json.legend = legend;
             json.series = series;
             $timeout(function(){
                 $scope.loading=false;
                 $scope.loading2=false;
                 $(document).ready(function() {
                     $('#container5').highcharts(json);
                 });
             },2000);
         });
     };
                 
                 $http.get('http://23.236.54.158:3000/revenue/avg/daily').
         success(function(response){
             console.log(response.data.length);
             var xAxis = {
                 type: 'datetime'
             };
             var series =  [
                 {
                     name: 'Average',
                     data: []
                 }
             ];
             var c=[];
             for(var m=0;m<response.data.length;m++){
                 c[m]=new Date(response.data[m].date);
                 series[0].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data[m].average]);
             }
             //        $scope.loading=false;
             var title = {
                 text: 'Average Daily Revenue'   
             };
             var subtitle = {
                 text: 'Source: Base_Address/revenue/daily'
             };        
             var yAxis = {
                 title: {
                     text: 'Amount (in $)'
                 },
                 plotLines: [{
                     value: 0,
                     width: 1,                              
                     color: '#808080'
                 }]
             };   
             var colors=['#004D75'];
             var tooltip = {
                 valueSuffix: '$'
             }

//        var legend = {
//            layout: 'vertical',
//            align: 'right',
//            verticalAlign: 'middle',
//            borderWidth: 0
//        };

             var json = {};
             json.title = title;
             json.subtitle = subtitle;
             json.xAxis = xAxis;
             json.yAxis = yAxis;
             json.colors=colors;
             json.tooltip = tooltip;
             //   json.legend = legend;
             json.series = series;
             $timeout(function(){
                 $scope.loading=false;
                 $scope.loading2=false;
                 $(document).ready(function() {
                     $('#container3').highcharts(json);
                 });
             },2000);
         });
////    =========================Country Revenue===================
//    $http.get('http://23.236.54.158:3000/country-revenue').
//    success(function(response){
//        console.log(response.data.length);
//        
//        
//   var chart = {
//      type: 'column'
//   };
//    var series= [{
//        name: 'Country Revenue',
//            data: []
//        }];     
//    var xAxis = {
//        categories: [],
//        crosshair: true
//   };
//        $scope.country_name=[];
//        for(var j=0;j<response.data.length;j++){
//             $http.get('https://restcountries.eu/rest/v1/alpha/'+response.data[j].country)
//            .success(function(response){
////                console.log(response.name);
//                $scope.country_name.push(response.name);
////                 console.log($scope.country_name);
//                 });
//        }
//        
//        $timeout(function(){
//                for(var i=0;i<response.data.length;i++){
//                     xAxis.categories[i]=$scope.country_name[i];
////                 console.log(xAxis);
//                    series[0].data.push(response.data[i].amount);
//                }
//        },6000);
//        
//   var title = {
//      text: 'Country Revenue'   
//   };
//   var subtitle = {
//      text: 'Base_Address/country-revenue'  
//   };
//   var yAxis = {
//      min: 0,
//      title: {
//         text: 'Amount (in $)'         
//      }      
//   };
//   var tooltip = {
//      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
//      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
//         '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
//      footerFormat: '</table>',
//      shared: true,
//      useHTML: true
//   };
//   var plotOptions = {
//      column: {
//         pointPadding: 0.2,
//         borderWidth: 0
//      }
//   };  
//   var credits = {
//      enabled: false
//   };
//    var colors=['#004D75'];
//      
//   var json = {};   
//   json.chart = chart; 
//   json.title = title;   
//   json.subtitle = subtitle; 
//   json.tooltip = tooltip;
//   json.xAxis = xAxis;
//   json.yAxis = yAxis;  
//   json.series = series;
//    json.colors=colors;
//   json.plotOptions = plotOptions;  
//   json.credits = credits;
//
//        $timeout(function(){
//             $scope.loading=false;
// $(document).ready(function() {
//   $('#country').highcharts(json);
//    
//});
//},8000);
//
//    });
////    ==================================map===============================
//    var data=[];
//
//        $http.get('http://23.236.54.158:3000/country-revenue')
//        .success(function(response){
//            console.log(response.data);
//            for(var i=0;i<response.data.length;i++){
//                data.push({
//                    "hc-key": response.data[i].country.toLowerCase(),
//                    "value": response.data[i].amount
//                });
//            }
//            console.log(data.length);
//            var title ={
//                text : 'Country Revenue'
//            };
//
//        var subtitle = {
//            text : 'Base_Address/country-revenue'
//        };
//
//        var mapNavigation= {
//            enabled: true,
//            buttonOptions: {
//                verticalAlign: 'bottom'
//            }
//        };
//
//        var colorAxis= {
//            min: 0
//        };
//
//        var series = [{
//            data : data,
//            mapData: Highcharts.maps['custom/world'],
//            joinBy: 'hc-key',
//            name: 'Revenue',
//            states: {
//                hover: {
//                    color: '#BADA55'
//                }
//            },
//            dataLabels: {
//                enabled: true,
//                format: '{point.name}'
//            }
//        }];
//    var json = {};   
//   json.title = title;   
//   json.subtitle = subtitle; 
//   json.mapNavigation = mapNavigation;
//   json.colorAxis = colorAxis;
//   json.series = series;  
//            $timeout(function(){
//                $scope.loading=false;
//                $(document).ready(function() {
//                    $('#country1').highcharts('Map',json);
//                });
//            },2000); 
//        });
});