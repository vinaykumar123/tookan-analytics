App.controller('TaskController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state, responseCode,$timeout) {
    $scope.loading=true;
    $scope.daily=true;
    $scope.week=false;
    $scope.month=false;
    
    $scope.getdaily=function(){
        $scope.daily=true;
        $scope.week=false;
        $scope.month=false;
    };
    
    $http.get('http://23.236.54.158:3000/tasks/daily').
    success(function(response){
        console.log(response.data.total.length);
        var xAxis = {
            type: 'datetime'
        };
        var series =  [
            {
                name: 'Pickup',
                data: []
            },
            {
                'name' :'Delivery',
                'data': []
            },
            {
                'name':'Appointment',
                'data': []
            },
            {
                'name':'Total',
                'data': []
            }
        ];
        var c=[];
        var d=[];
        var e=[];
        var f=[];
        for(var m=0;m<response.data.pickup.length;m++){
            c[m]=new Date(response.data.pickup[m].date);
            series[0].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data.pickup[m].count]);
        }
        for(var m=0;m<response.data.delivery.length;m++){
            d[m]=new Date(response.data.delivery[m].date);
            series[1].data.push([Date.UTC(d[m].getFullYear(),d[m].getMonth(),d[m].getDate()),response.data.delivery[m].count]);
        }
        for(var m=0;m<response.data.appointment.length;m++){
            e[m]=new Date(response.data.appointment[m].date);
            series[2].data.push([Date.UTC(e[m].getFullYear(),e[m].getMonth(),e[m].getDate()),response.data.appointment[m].count]);
        }
        for(var m=0;m<response.data.pickup.length;m++){
            f[m]=new Date(response.data.pickup[m].date);
            series[3].data.push([Date.UTC(f[m].getFullYear(),f[m].getMonth(),f[m].getDate()),response.data.pickup[m].count]);
        }
        var title = {
            text: 'Daily Tasks'   
        };
        var subtitle = {
            text: 'Source: Base_Address/tasks/daily'
        };
        var yAxis = {
            title: {
                text: 'Count'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        };   
        var colors=['#004D75','green','red','lightblue'];
        var tooltip = {
            shared: true,
            valueSuffix: 'count',
            crosshairs: true
        };

//        var legend = {
//            layout: 'vertical',
//            align: 'right',
//            verticalAlign: 'middle',
//            borderWidth: 0
//        };
        var json = {};
        json.title = title;
        json.subtitle = subtitle;
        json.xAxis = xAxis;
        json.yAxis = yAxis;
        json.colors=colors;
        json.tooltip = tooltip;
        //   json.legend = legend;
        json.series = series;
        $timeout(function(){
            $scope.loading=false;
            $scope.loading1=false;
            $(document).ready(function() {
                $('#container').highcharts(json);
            });
        },2000);
    });
    
    $scope.getweekly=function(){
        $scope.loading1 = true;
        $scope.daily=false;
        $scope.week=true;
        $scope.month=false;
        //    =====================================tasks weekly======================
        $http.get('http://23.236.54.158:3000/tasks/weekly').
        success(function(response){
            console.log(response.data.total.length);
            var xAxis = {
                type: 'datetime'
            };
            var series =  [
                {
                    name: 'Pickup',
                    data: []
                },
                {
                    'name' :'Delivery',
                    'data': []
                },
                {
                    'name':'Appointment',
                    'data': []
                },
                {
                    'name':'Total',
                    'data': []
                }
            ];
            var c=[];
            var d=[];
            var e=[];
            var f=[];
            for(var m=0;m<response.data.pickup.length;m++){
                c[m]=new Date(response.data.pickup[m].date);
                series[0].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data.pickup[m].count]);
            }
            for(var m=0;m<response.data.delivery.length;m++){
                d[m]=new Date(response.data.delivery[m].date);
                series[1].data.push([Date.UTC(d[m].getFullYear(),d[m].getMonth(),d[m].getDate()),response.data.delivery[m].count]);
            }
            for(var m=0;m<response.data.appointment.length;m++){
                e[m]=new Date(response.data.appointment[m].date);
            series[2].data.push([Date.UTC(e[m].getFullYear(),e[m].getMonth(),e[m].getDate()),response.data.appointment[m].count]);
            }
            for(var m=0;m<response.data.pickup.length;m++){
                f[m]=new Date(response.data.pickup[m].date);
                series[3].data.push([Date.UTC(f[m].getFullYear(),f[m].getMonth(),f[m].getDate()),response.data.pickup[m].count]);
            }
            var title = {
                text: 'Weekly Tasks'   
            };
            var subtitle = {
                text: 'Source: Base_Address/tasks/weekly'
            };
            var yAxis = {
                title: {
                    text: 'Count'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            };   
            var colors=['#004D75','green','red','lightblue'];
            var tooltip = {
                shared: true,
                valueSuffix: 'count',
                crosshairs: true
            };

//        var legend = {
//            layout: 'vertical',
//            align: 'right',
//            verticalAlign: 'middle',
//            borderWidth: 0
//        };
            var json = {};
            json.title = title;
            json.subtitle = subtitle;
            json.xAxis = xAxis;
            json.yAxis = yAxis;
            json.colors=colors;
            json.tooltip = tooltip;
            //   json.legend = legend;
            json.series = series;
            $timeout(function(){
                $scope.loading=false;
                $scope.loading1=false;
                $(document).ready(function() {
                    $('#container1').highcharts(json);
                });
            },2000);
        });
    };
    
    $scope.getmonthly=function(){
        $scope.loading1 = true;
        $scope.daily=false;
        $scope.week=false;
        $scope.month=true;
        //    =====================================tasks monthly======================
        $http.get('http://23.236.54.158:3000/tasks/monthly').
        success(function(response){
            console.log(response.data.total.length);
            var xAxis = {
                type: 'datetime'
            };
            var series =  [
                {
                    name: 'Pickup',
                    data: []
                },
                {
                    'name' :'Delivery',
                    'data': []
                },
                {
                    'name':'Appointment',
                    'data': []
                },
                {
                    'name':'Total',
                    'data': []
                }
            ];
            
            var c=[];
            var d=[];
            var e=[];
            var f=[];
            for(var m=0;m<response.data.pickup.length;m++){
                c[m]=new Date(response.data.pickup[m].date);
                series[0].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data.pickup[m].count]);
            }
            for(var m=0;m<response.data.delivery.length;m++){
                d[m]=new Date(response.data.delivery[m].date);
                series[1].data.push([Date.UTC(d[m].getFullYear(),d[m].getMonth(),d[m].getDate()),response.data.delivery[m].count]);
            }
            for(var m=0;m<response.data.appointment.length;m++){
                e[m]=new Date(response.data.appointment[m].date);
            series[2].data.push([Date.UTC(e[m].getFullYear(),e[m].getMonth(),e[m].getDate()),response.data.appointment[m].count]);
            }
            for(var m=0;m<response.data.pickup.length;m++){
                f[m]=new Date(response.data.pickup[m].date);
                series[3].data.push([Date.UTC(f[m].getFullYear(),f[m].getMonth(),f[m].getDate()),response.data.pickup[m].count]);
            }
            var title = {
                text: 'Monthly Tasks'   
            };
            var subtitle = {
                text: 'Source: Base_Address/tasks/monthly'
            };
        
            var yAxis = {
                title: {
                    text: 'Count'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            };   
        
            var colors=['#004D75','green','red','lightblue'];
            var tooltip = {
                shared: true,
                valueSuffix: 'count',
                crosshairs: true
            };

//        var legend = {
//            layout: 'vertical',
//            align: 'right',
//            verticalAlign: 'middle',
//            borderWidth: 0
//        };

            var json = {};
            json.title = title;
            json.subtitle = subtitle;
            json.xAxis = xAxis;
            json.yAxis = yAxis;
            json.colors=colors;
            json.tooltip = tooltip;
            //   json.legend = legend;
            json.series = series;
            $timeout(function(){
                $scope.loading=false;
                $scope.loading1=false;
                $(document).ready(function() {
                    $('#container2').highcharts(json);
                });
            },2000);
        });
    };
});