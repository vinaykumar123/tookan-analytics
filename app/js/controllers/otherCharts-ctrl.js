App.controller('otherController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state, responseCode,$timeout) {
    $scope.loading=true;
//    $scope.loading1=true;
//    $scope.loading2=true;
//    $scope.loading3=true;
//    $scope.loading4=true;
//    $scope.loading5=true;
    $scope.chart_flag=false;
    $http.get('http://23.236.54.158:3000/daily-signups')
        .success(function(response){
        console.log(response.data.length);
        var xAxis = {
             type: 'datetime'
        };
        
        var series =  [
            {
                name: 'Daily SignUps',
                data: []
            }];
       
        var get_date=[];
         for(var m=0;m<response.data.length;m++){
          get_date[m]=new Date(response.data[m].date);
        series[0].data.push([Date.UTC(get_date[m].getFullYear(),get_date[m].getMonth(),get_date[m].getDate())
                             ,response.data[m].users]);
        }
        var title = {
            text: 'Daily SignUps'   
        };
        var subtitle = {
            text: 'Source: Base_Address/daily-signups'
        };
        
        var yAxis = {
            title: {
                text: 'No. Of Users'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        };   

//        var tooltip = {
//            valueSuffix: '$'
//        }

//        var legend = {
//            layout: 'vertical',
//            align: 'bottom',
//            verticalAlign: 'middle',
//            borderWidth: 0
//        };
        var colors=['#004D75'];

   var json = {};

   json.title = title;
   json.subtitle = subtitle;
   json.xAxis = xAxis;
   json.yAxis = yAxis;
        json.colors=colors;
//   json.tooltip = tooltip;
//   json.legend = legend;
   json.series = series;

        $timeout(function(){
             $scope.loading=false;
 $(document).ready(function() {
   $('#container').highcharts(json);
});
},2000);

    });
    
//    =========================Active Users===================
    $http.get('http://23.236.54.158:3000/active-users').
    success(function(response){
        console.log(response.data.length);
        
        
      var xAxis = {
             type: 'datetime'
         };
        var series =  [
      {
         name: 'Active Users',
         data: []
      }
   ];
         var get_date1=[];
         for(var m=0;m<response.data.length;m++){
          get_date1[m]=new Date(response.data[m].date);
        series[0].data.push([Date.UTC(get_date1[m].getFullYear(),get_date1[m].getMonth(),get_date1[m].getDate())
                             ,response.data[m].active_users]);
        }
        var title = {
            text: 'Active Users'   
        };
        var subtitle = {
            text: 'Source: Base_Address/active-users'
        };
        
        var yAxis = {
            title: {
                text: 'No. Of active users'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        };   

//        var tooltip = {
//            valueSuffix: '$'
//        }

//        var legend = {
//            layout: 'vertical',
//            align: 'right',
//            verticalAlign: 'middle',
//            borderWidth: 0
//        };
        var colors=['#004D75'];

   var json = {};

   json.title = title;
   json.subtitle = subtitle;
   json.xAxis = xAxis;
   json.yAxis = yAxis;
//   json.tooltip = tooltip;
//   json.legend = legend;
        json.colors=colors;
   json.series = series;

        $timeout(function(){
             $scope.loading=false;
 $(document).ready(function() {
   $('#container1').highcharts(json);
});
},2000);
    });
//    ====================ends active=========================
    
    //    =========================Daily Tasks===================
    $http.get('http://23.236.54.158:3000/daily-tasks').
    success(function(response){
//        console.log(response.data.length);
        
        
      var chart = {
      type: 'area'
   };
   var title = {
      text: 'Daily Tasks'   
   };    
        var subtitle = {
            text: 'Source: Base_Address/daily-tasks'
        };
   var xAxis = {
       type: 'datetime'
   };
        var series= [{
            name: 'Pick Up',
            data: []
        }, {
            name: 'Delivery',
            data: []
        }, {
            name: 'Appointment',
            data: []      
   }];     
        var a=[];
        var b=[];
        var c=[];
//        for(var i=0;i<response.data.total.length;i++){
//          
//            xAxis.categories[i]=;
//        }
        for(var j=0;j<response.data.pickup.length;j++){
              a[j]=new Date(response.data.pickup[j].date);
            series[0].data.push([Date.UTC(a[j].getFullYear(),a[j].getMonth(),a[j].getDate()),response.data.pickup[j].count]);
        }
        for(var k=0;k<response.data.delivery.length;k++){
             b[k]=new Date(response.data.delivery[k].date);
             series[1].data.push([Date.UTC(b[k].getFullYear(),b[k].getMonth(),b[k].getDate()),response.data.delivery[k].count]);
        }
        for(var m=0;m<response.data.appointment.length;m++){
          c[m]=new Date(response.data.appointment[m].date);
             series[2].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data.appointment[m].count]);
        }
    var yAxis = {
      title: {
         text: 'Total Count'
      }
   };
   var tooltip = {
      shared: true,
      valueSuffix: ' count'
   };
   var plotOptions = {
      area: {
         stacking: 'normal',
         lineColor: '#666666',
         lineWidth: 1,
         marker: {
            lineWidth: 1,
            lineColor: '#666666'
         }
      }
   };
//         var plotOptions= {
//            spline: {
//                marker: {
//                    enabled: true
//                }
//            }
//        };
   var credits = {
      enabled: true
   };
      
   var json = {};   
   json.chart = chart; 
   json.title = title; 
   json.subtitle = subtitle; 
   json.xAxis = xAxis;
   json.yAxis = yAxis;
   json.tooltip = tooltip;
   json.plotOptions = plotOptions;
   json.credits = credits;
   json.series = series;
        $timeout(function(){
             $scope.loading=false;
 $(document).ready(function() {
   $('#container2').highcharts(json);
});
},2000);
    });
//    ====================ends daily tasks=========================
    //    =========================Weekly Growth by value===================
    $http.get('http://23.236.54.158:3000/weekly-growth').
    success(function(response){
        var title = {
            text: 'Weekly Growth by Value'   
        };
        
        var subtitle = {
            text: 'Base_Address/weekly-growth'
        };
        var xAxis = {
            type: 'datetime'
        };
        var series =  [
      {
         name: 'Completed',
         data: []
      }, 
      {
         name: 'Created',
         data: []
      }
   ];
        var get_date2=[];
        var get_date3=[];
        for(var j=0;j<response.data.completed.length;j++){
              get_date2[j]=new Date(response.data.completed[j].date);
            series[0].data.push([Date.UTC(get_date2[j].getFullYear(),get_date2[j].getMonth(),get_date2[j].getDate()),
                                 response.data.completed[j].value]);
        }
         for(var k=0;k<response.data.created.length;k++){
              get_date3[k]=new Date(response.data.created[k].date);
            series[1].data.push([Date.UTC(get_date3[k].getFullYear(),get_date3[k].getMonth(),get_date3[k].getDate()),
                                 response.data.created[k].value]);
        }
        var yAxis = {
            title: {
                text: 'Value'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        };   

        var tooltip = {
            crosshairs: true,
            shared:true
        };


   var json = {};  
   json.title = title;
   json.subtitle = subtitle;
   json.xAxis = xAxis;
   json.yAxis = yAxis;
   json.tooltip = tooltip;
   json.series = series;

        $timeout(function(){
             $scope.loading=false;
 $(document).ready(function() {
   $('#container3').highcharts(json);
});
},2000);
    });
//    ====================ends weekly growth=========================
    //    =========================Weekly Growth by percent===================
    $http.get('http://23.236.54.158:3000/weekly-growth').
    success(function(response){
        console.log(response.data.length);
        var title = {
            text: 'Weekly Growth by Percentage'   
        };
        
        var subtitle = {
            text: 'Base_Address/weekly-growth'
        };
        var xAxis = {
            type: 'datetime'
        };
        var series =  [
      {
         name: 'Completed',
         data: []
      }, 
      {
         name: 'Created',
         data: []
      }
   ];
        var get_date2=[];
        var get_date3=[];
        for(var j=0;j<response.data.completed.length;j++){
              get_date2[j]=new Date(response.data.completed[j].date);
            series[0].data.push([Date.UTC(get_date2[j].getFullYear(),get_date2[j].getMonth(),get_date2[j].getDate()),
                                 response.data.completed[j].growth]);
        }
         for(var k=0;k<response.data.created.length;k++){
              get_date3[k]=new Date(response.data.created[k].date);
            series[1].data.push([Date.UTC(get_date3[k].getFullYear(),get_date3[k].getMonth(),get_date3[k].getDate()),
                                 response.data.created[k].growth]);
        }
        var yAxis = {
            title: {
                text: 'Growth in %age'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        };   

        var tooltip = {
            valueSuffix: '%',
            crosshairs: true,
            shared:true
        };


   var json = {};  
   json.title = title;
   json.subtitle = subtitle;
   json.xAxis = xAxis;
   json.yAxis = yAxis;
   json.tooltip = tooltip;
   json.series = series;

        $timeout(function(){
             $scope.loading=false;
 $(document).ready(function() {
   $('#container7').highcharts(json);
});
},2000);
    });
//    ====================ends weekly growth=========================
    
//    ========================daily Task States=============================
    $http.get('http://23.236.54.158:3000/daily-task-states').
    success(function(response){
//        console.log(response.data.length);
        
         var xAxis = {
            type: 'datetime'
         };
        var series =  [
      {
         name: 'Started',
         data: []
      },
            {
         name: 'Cancelled',
         data: []
      },
            {
         name: 'Completed',
         data: []
      },
            {
         name: 'Accepted',
         data: []
      },
            {
         name: 'Failed',
         data: []
      },
            {
         name: 'Arrived',
         data: []
      },
            {
         name: 'Partial',
         data: []
      },
            {
         name: 'Declined',
         data: []
      },
            {
         name: 'Assigned',
         data: []
      },
            {
         name: 'Ignored',
         data: []
      }
   ];
       
         var get_date4=[];
        var get_date5=[];
        var get_date6=[];
        var get_date7=[];
        var get_date8=[];
        var get_date9=[];
        var get_date_1=[];
        var get_date_2=[];
        var get_date_3=[];
        var get_date_4=[];
        for(var j=0;j<response.data.started.length;j++){
              get_date4[j]=new Date(response.data.started[j].date);
            series[0].data.push([Date.UTC(get_date4[j].getFullYear(),get_date4[j].getMonth(),get_date4[j].getDate()),
                                 response.data.started[j].count]);
        }
         for(var j=0;j<response.data.cancelled.length;j++){
              get_date5[j]=new Date(response.data.cancelled[j].date);
            series[1].data.push([Date.UTC(get_date5[j].getFullYear(),get_date5[j].getMonth(),get_date5[j].getDate()),
                                 response.data.cancelled[j].count]);
        }
        
         for(var j=0;j<response.data.completed.length;j++){
              get_date6[j]=new Date(response.data.completed[j].date);
            series[2].data.push([Date.UTC(get_date6[j].getFullYear(),get_date6[j].getMonth(),get_date6[j].getDate()),
                                 response.data.completed[j].count]);
        }
        
         for(var j=0;j<response.data.accepted.length;j++){
              get_date7[j]=new Date(response.data.accepted[j].date);
            series[3].data.push([Date.UTC(get_date7[j].getFullYear(),get_date7[j].getMonth(),get_date7[j].getDate()),
                                 response.data.accepted[j].count]);
        }
        
         for(var j=0;j<response.data.failed.length;j++){
              get_date8[j]=new Date(response.data.failed[j].date);
            series[4].data.push([Date.UTC(get_date8[j].getFullYear(),get_date8[j].getMonth(),get_date8[j].getDate()),
                                 response.data.failed[j].count]);
        }
        
         for(var j=0;j<response.data.arrived.length;j++){
              get_date9[j]=new Date(response.data.arrived[j].date);
            series[5].data.push([Date.UTC(get_date9[j].getFullYear(),get_date9[j].getMonth(),get_date9[j].getDate()),
                                 response.data.arrived[j].count]);
        }
        
         for(var j=0;j<response.data.partial.length;j++){
              get_date_1[j]=new Date(response.data.partial[j].date);
            series[6].data.push([Date.UTC(get_date_1[j].getFullYear(),get_date_1[j].getMonth(),get_date_1[j].getDate()),
                                 response.data.partial[j].count]);
        }
        
         for(var j=0;j<response.data.declined.length;j++){
              get_date_2[j]=new Date(response.data.declined[j].date);
            series[7].data.push([Date.UTC(get_date_2[j].getFullYear(),get_date_2[j].getMonth(),get_date_2[j].getDate()),
                                 response.data.declined[j].count]);
        }
        
         for(var j=0;j<response.data.assigned.length;j++){
              get_date_3[j]=new Date(response.data.assigned[j].date);
            series[8].data.push([Date.UTC(get_date_3[j].getFullYear(),get_date_3[j].getMonth(),get_date_3[j].getDate()),
                                 response.data.assigned[j].count]);
        }
        
         for(var j=0;j<response.data.ignored.length;j++){
              get_date_4[j]=new Date(response.data.ignored[j].date);
            series[9].data.push([Date.UTC(get_date_4[j].getFullYear(),get_date_4[j].getMonth(),get_date_4[j].getDate()),
                                 response.data.ignored[j].count]);
        }
        
        var title = {
            text: 'Daily Task By Different States'   
        };
        var subtitle = {
            text: 'Source: Base_Address/daily-task-states'
        };
        
        var yAxis = {
            title: {
                text: 'No. Of Users'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        };   

       var tooltip = {
      crosshairs: true,
      shared: true
   };

//        var legend = {
//            layout: 'vertical',
//            align: 'bottom',
//            verticalAlign: 'middle',
//            borderWidth: 0
//        };
        var colors=['#004D75','green','red','lightblue','lightgreen','#38d3e3','brown','yellow','orange',"#ff6600"];

   var json = {};

   json.title = title;
   json.subtitle = subtitle;
   json.xAxis = xAxis;
   json.yAxis = yAxis;
        json.colors=colors;
   json.tooltip = tooltip;
//   json.legend = legend;
   json.series = series;

        $timeout(function(){
             $scope.loading=false;
 $(document).ready(function() {
   $('#container4').highcharts(json);
});
},2000);

    });
//    =================states ends======================
    
    //    =========================Geographical Signups===================
    $http.get('http://23.236.54.158:3000/geographical-signups').
    success(function(response){
        console.log(response.data.length);
        
        
   var chart = {
      type: 'column'
   };
    var series= [{
        name: 'Users',
            data: []
        }];     
    var xAxis = {
        categories: [],
        crosshair: true
   };
        $scope.country_name=[];
        for(var j=0;j<response.data.length;j++){
            
            if(response.data[j].country_phone_code=='+91'){
                var get_code=response.data[j].country_phone_code.split('+')[1];
                    console.log(get_code);
                 $http.get('https://restcountries.eu/rest/v1/callingcode/'+get_code)
            .success(function(response){
                console.log(response[0].name);
                $scope.country_name.push(response[0].name);
//                 console.log($scope.country_name);
                 });
            }
               else if(response.data[j].country_phone_code=='91'){
                     $http.get('https://restcountries.eu/rest/v1/callingcode/'+response.data[j].country_phone_code)
            .success(function(response){
                console.log(response[0].name);
                $scope.country_name.push(response[0].name);
//                 console.log($scope.country_name);
                 });
                
            }
            
            else{
             $http.get('https://restcountries.eu/rest/v1/alpha/'+response.data[j].country_phone_code)
            .success(function(response){
//                console.log(response.name);
                $scope.country_name.push(response.name);
//                 console.log($scope.country_name);
                 });
            }
        }
       
        
        $timeout(function(){
             console.log($scope.country_name.length);
                for(var i=0;i<response.data.length;i++){
                     xAxis.categories[i]=$scope.country_name[i];
//                 console.log(xAxis);
                    series[0].data.push(response.data[i].users);
                }
        },10000);
        
   var title = {
      text: 'Geogrphical Signups'   
   };
   var subtitle = {
      text: 'Base_Address/geographical-signups'  
   };
   var yAxis = {
      min: 0,
      title: {
         text: 'No. of Users'         
      }      
   };
   var tooltip = {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
         '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
   };
   var plotOptions = {
      column: {
         pointPadding: 0.2,
         borderWidth: 0
      }
   };  
   var credits = {
      enabled: false
   };
    var colors=['#004D75'];
      
   var json = {};   
   json.chart = chart; 
   json.title = title;   
   json.subtitle = subtitle; 
   json.tooltip = tooltip;
   json.xAxis = xAxis;
   json.yAxis = yAxis;  
   json.series = series;
    json.colors=colors;
   json.plotOptions = plotOptions;  
   json.credits = credits;

        $timeout(function(){
             $scope.loading=false;
 $(document).ready(function() {
   $('#container5').highcharts(json);
    
});
},12000);

    });
    
     /*--------------------------------------------------------------------------
     * ---------------- code for datepicker ------------------------------------
     --------------------------------------------------------------------------*/

    $scope.maxDate = new Date();

    $scope.today = function() {
        $scope.promo.start_date = new Date();
    };
    //$scope.today();

    $scope.clear = function () {
        $scope.promo.start_date = null;
    };

//    $scope.toggleMin = function() {
//        $scope.maxDate = $scope.maxDate ? null : new Date();
//    };
//    $scope.toggleMin();

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.initDate = new Date();
    $scope.format = 'yyyy-MM-dd';



    $scope.today = function() {
        $scope.promo.end_date = new Date();
    };
    //$scope.today();

    $scope.clear = function () {
        $scope.promo.end_date = null;
    };

    $scope.open1 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened1 = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.initDate = new Date();
    $scope.format = 'yyyy-MM-dd';
//    ==================date picker ends======================
    
     $scope.findCharts = function () {
         $scope.loading6=true;
         $scope.chart_flag=true;
        var start_date = moment($scope.reports.start_date1).format('YYYY-MM-DD');
        var end_date = moment($scope.reports.end_date1).format('YYYY-MM-DD');
         console.log(start_date,end_date);
         
         $http.get('http://23.236.54.158:3000/date-signups/'+start_date+"/"+end_date)
         .success(function(response){
             console.log(response.data.length);
              var xAxis = {
             type: 'datetime'
         };
        var series =  [
      {
         name: 'Users',
         data: []
      }
   ];
       
              var get_date_5=[];
        for(var j=0;j<response.data.length;j++){
              get_date_5[j]=new Date(response.data[j].date);
            series[0].data.push([Date.UTC(get_date_5[j].getFullYear(),get_date_5[j].getMonth(),get_date_5[j].getDate()),
                                 response.data[j].users]);
        }
        var title = {
            text: 'Date According SignUps'   
        };
        var subtitle = {
            text: 'Source: Base_Address/date-signups/start_date/end_date'
        };
        
        var yAxis = {
            title: {
                text: 'No. Of Users'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        };   

//        var tooltip = {
//            valueSuffix: '$'
//        }

//        var legend = {
//            layout: 'vertical',
//            align: 'bottom',
//            verticalAlign: 'middle',
//            borderWidth: 0
//        };
        var colors=['#004D75'];

   var json = {};

   json.title = title;
   json.subtitle = subtitle;
   json.xAxis = xAxis;
   json.yAxis = yAxis;
        json.colors=colors;
//   json.tooltip = tooltip;
//   json.legend = legend;
   json.series = series;

        $timeout(function(){
             $scope.loading6=false;
 $(document).ready(function() {
   $('#container6').highcharts(json);
});
},2000);

         });
     };
});