App.controller('UserController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state, responseCode,$timeout) {
    $scope.loading=true;
    $scope.daily=true;
    $scope.week=false;
    $scope.month=false;
    $scope.dailybill=true;
    $scope.weekbill=false;
    $scope.monthbill=false;
    $scope.dailyclosure=true;
    $scope.weekclosure=false;
    $scope.monthclosure=false;
    
    $scope.getdaily=function(){
        $scope.daily=true;
        $scope.week=false;
        $scope.month=false;
    };
    $scope.getdailybill=function(){
        $scope.dailybill=true;
        $scope.weekbill=false;
        $scope.monthbill=false;
    };
    $scope.getdailyclosure=function(){
        $scope.dailyclosure=true;
        $scope.weekclosure=false;
        $scope.monthclosure=false;
    };
//    =======================active users========================
    $http.get('http://23.236.54.158:3000/users/active/daily').
         success(function(response){
             console.log(response.data.length);
             var xAxis = {
                 type: 'datetime'
             };
             var series =  [
                 {
                     name: 'Users',
                     data: []
                 }
             ];
             var c=[];
             for(var m=0;m<response.data.length;m++){
                 c[m]=new Date(response.data[m].date);
                 series[0].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data[m].active_users]);
             }
             //        $scope.loading=false;
             var title = {
                 text: 'Daily Active Users'   
             };
             var subtitle = {
                 text: 'Source: Base_Address/users/active/daily'
             };        
             var yAxis = {
                 title: {
                     text: 'No. Of active users'
                 },
                 plotLines: [{
                     value: 0,
                     width: 1,                              
                     color: '#808080'
                 }]
             };   
             var colors=['#004D75'];
//             var tooltip = {
//                 valueSuffix: '$'
//             }

//        var legend = {
//            layout: 'vertical',
//            align: 'right',
//            verticalAlign: 'middle',
//            borderWidth: 0
//        };

             var json = {};
             json.title = title;
             json.subtitle = subtitle;
             json.xAxis = xAxis;
             json.yAxis = yAxis;
             json.colors=colors;
//             json.tooltip = tooltip;
             //   json.legend = legend;
             json.series = series;
             $timeout(function(){
                 $scope.loading=false;
                 $scope.loading1=false;
                 $(document).ready(function() {
                     $('#container').highcharts(json);
                 });
             },2000);
         });
//    ========================billed users daily==========================
    $http.get('http://23.236.54.158:3000/users/billed/daily').
         success(function(response){
             console.log(response.data.length);
             var xAxis = {
                 type: 'datetime'
             };
             var series =  [
                 {
                     name: 'Users',
                     data: []
                 }
             ];
             var c=[];
             for(var m=0;m<response.data.length;m++){
                 c[m]=new Date(response.data[m].date);
                 series[0].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data[m].users]);
             }
             //        $scope.loading=false;
             var title = {
                 text: 'Users Paid Daily'   
             };
             var subtitle = {
                 text: 'Source: Base_Address/users/billed/daily'
             };        
             var yAxis = {
                 title: {
                     text: 'No. Of users'
                 },
                 plotLines: [{
                     value: 0,
                     width: 1,                              
                     color: '#808080'
                 }]
             };   
             var colors=['#004D75'];
//             var tooltip = {
//                 valueSuffix: '$'
//             }

//        var legend = {
//            layout: 'vertical',
//            align: 'right',
//            verticalAlign: 'middle',
//            borderWidth: 0
//        };

             var json = {};
             json.title = title;
             json.subtitle = subtitle;
             json.xAxis = xAxis;
             json.yAxis = yAxis;
             json.colors=colors;
//             json.tooltip = tooltip;
             //   json.legend = legend;
             json.series = series;
             $timeout(function(){
                 $scope.loading=false;
                 $scope.loading2=false;
                 $(document).ready(function() {
                     $('#container3').highcharts(json);
                 });
             },2000);
         });
    //    =======================users closure daily========================
    $http.get('http://23.236.54.158:3000/users/closure/daily').
         success(function(response){
             console.log(response.data.length);
             var xAxis = {
                 type: 'datetime'
             };
             var series =  [
                 {
                     name: 'Ratio',
                     data: []
                 }
             ];
             var c=[];
             for(var m=0;m<response.data.length;m++){
                 c[m]=new Date(response.data[m].date);
                 series[0].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data[m].ratio]);
             }
             //        $scope.loading=false;
             var title = {
                 text: 'Daily Users Closure'   
             };
             var subtitle = {
                 text: 'Source: Base_Address/users/closure/daily'
             };        
             var yAxis = {
                 title: {
                     text: 'Ratio ( Business/Paid )'
                 },
                 plotLines: [{
                     value: 0,
                     width: 1,                              
                     color: '#808080'
                 }]
             };   
             var colors=['#004D75'];
//             var tooltip = {
//                 valueSuffix: '$'
//             }

//        var legend = {
//            layout: 'vertical',
//            align: 'right',
//            verticalAlign: 'middle',
//            borderWidth: 0
//        };

             var json = {};
             json.title = title;
             json.subtitle = subtitle;
             json.xAxis = xAxis;
             json.yAxis = yAxis;
             json.colors=colors;
//             json.tooltip = tooltip;
             //   json.legend = legend;
             json.series = series;
             $timeout(function(){
                 $scope.loading=false;
                 $scope.loading3=false;
                 $(document).ready(function() {
                     $('#container6').highcharts(json);
                 });
             },2000);
         });
    
     $scope.getweekly=function(){
        $scope.loading1 = true;
        $scope.daily=false;
        $scope.week=true;
        $scope.month=false;
        
        //    ======================weekly active users=====================
        $http.get('http://23.236.54.158:3000/users/active/weekly').
        success(function(response){
            console.log(response.data.length);
            var xAxis = {
                type: 'datetime'
            };
            var series =  [{
                name: 'Users',
                data: []
            }];
            
            var c=[];
            for(var m=0;m<response.data.length;m++){
                c[m]=new Date(response.data[m].date);
                series[0].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data[m].active_users]);
            }
            var title = {
                text: 'Weekly Active Users'   
            };
            var subtitle = {
                text: 'Source: Base_Address/users/active/weekly'
            };
            var yAxis = {
                title: {
                    text: 'No. of active users'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]};   
            var colors=['#004D75'];
//            var tooltip = {
//                valueSuffix: '$'
//            };

//        var legend = {
//            layout: 'vertical',
//            align: 'right',
//            verticalAlign: 'middle',
//            borderWidth: 0
//        };
            var json = {};
            json.title = title;
            json.subtitle = subtitle;
            json.xAxis = xAxis;
            json.yAxis = yAxis;
            json.colors=colors;
//            json.tooltip = tooltip;
            //   json.legend = legend;
            json.series = series;
            $timeout(function(){
                $scope.loading=false;
                $scope.loading1=false;
                $(document).ready(function() {
                    $('#container1').highcharts(json);
                });
            },2000);
        });
    };
    
     $scope.getmonthly=function(){
        $scope.loading1 = true;
        $scope.daily=false;
        $scope.week=false;
        $scope.month=true;
         //    ===========================monthly active users===================
         $http.get('http://23.236.54.158:3000/users/active/monthly').
         success(function(response){
             console.log(response.data.length);
             var xAxis = {
                 type: 'datetime'
             };
             var series =  [{
                 name: 'Users',
                 data: []
             }];
             var c=[];
             for(var m=0;m<response.data.length;m++){
                 c[m]=new Date(response.data[m].date);
                 series[0].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data[m].active_users]);
             }
             var title = {
                 text: 'Monthly Active Users Monthly'   
             };
             var subtitle = {
                 text: 'Source: Base_Address/users/active/monthly'
             };
             var yAxis = {
                 title: {
                     text: 'No. of active users'
                 },
                 plotLines: [{
                     value: 0,
                     width: 1,
                     color: '#808080'
                 }]};   
             var colors=['#004D75'];
//             var tooltip = {
//                 valueSuffix: '$'
//             };
             //        var legend = {
             //            layout: 'vertical',
             //            align: 'right',
             //            verticalAlign: 'middle',
             //            borderWidth: 0
             //        };
             
             var json = {};
             json.title = title;
             json.subtitle = subtitle;
             json.xAxis = xAxis;
             json.yAxis = yAxis;
             json.colors=colors;
//             json.tooltip = tooltip;
             //   json.legend = legend;
             json.series = series;
             $timeout(function(){
                 $scope.loading=false;
                 $scope.loading1=false;
                 $(document).ready(function() {
                     $('#container2').highcharts(json);
                 });
             },2000);
         });
     };
    
     $scope.getweeklybill=function(){
        $scope.loading2 = true;
        $scope.dailybill=false;
        $scope.weekbill=true;
        $scope.monthbill=false;
        
        //    ======================weekly billed users=====================
        $http.get('http://23.236.54.158:3000/users/billed/weekly').
        success(function(response){
            console.log(response.data.length);
            var xAxis = {
                type: 'datetime'
            };
            var series =  [{
                name: 'Users',
                data: []
            }];
            
            var c=[];
            for(var m=0;m<response.data.length;m++){
                c[m]=new Date(response.data[m].date);
                series[0].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data[m].users]);
            }
            var title = {
                text: 'Users Paid Weekly'   
            };
            var subtitle = {
                text: 'Source: Base_Address/users/billed/weekly'
            };
            var yAxis = {
                title: {
                    text: 'No. of users'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]};   
            var colors=['#004D75'];
//            var tooltip = {
//                valueSuffix: '$'
//            };

//        var legend = {
//            layout: 'vertical',
//            align: 'right',
//            verticalAlign: 'middle',
//            borderWidth: 0
//        };
            var json = {};
            json.title = title;
            json.subtitle = subtitle;
            json.xAxis = xAxis;
            json.yAxis = yAxis;
            json.colors=colors;
//            json.tooltip = tooltip;
            //   json.legend = legend;
            json.series = series;
            $timeout(function(){
                $scope.loading=false;
                $scope.loading2=false;
                $(document).ready(function() {
                    $('#container4').highcharts(json);
                });
            },2000);
        });
    };
    
     $scope.getmonthlybill=function(){
        $scope.loading2 = true;
        $scope.dailybill=false;
        $scope.weekbill=false;
        $scope.monthbill=true;
         //    ===========================monthly revenue===================
         $http.get('http://23.236.54.158:3000/users/billed/monthly').
         success(function(response){
             console.log(response.data.length);
             var xAxis = {
                 type: 'datetime'
             };
             var series =  [{
                 name: 'Users',
                 data: []
             }];
             var c=[];
             for(var m=0;m<response.data.length;m++){
                 c[m]=new Date(response.data[m].date);
                 series[0].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data[m].users]);
             }
             var title = {
                 text: 'Users Paid Monthly'   
             };
             var subtitle = {
                 text: 'Source: Base_Address/users/billed/monthly'
             };
             var yAxis = {
                 title: {
                     text: 'No. of users'
                 },
                 plotLines: [{
                     value: 0,
                     width: 1,
                     color: '#808080'
                 }]};   
             var colors=['#004D75'];
//             var tooltip = {
//                 valueSuffix: '$'
//             };
             //        var legend = {
             //            layout: 'vertical',
             //            align: 'right',
             //            verticalAlign: 'middle',
             //            borderWidth: 0
             //        };
             
             var json = {};
             json.title = title;
             json.subtitle = subtitle;
             json.xAxis = xAxis;
             json.yAxis = yAxis;
             json.colors=colors;
//             json.tooltip = tooltip;
             //   json.legend = legend;
             json.series = series;
             $timeout(function(){
                 $scope.loading=false;
                 $scope.loading2=false;
                 $(document).ready(function() {
                     $('#container5').highcharts(json);
                 });
             },2000);
         });
     };
    $scope.getweeklyclosure=function(){
        $scope.loading3 = true;
        $scope.dailyclosure=false;
        $scope.weekclosure=true;
        $scope.monthclosure=false;
        
        //    ======================weekly closure users=====================
        $http.get('http://23.236.54.158:3000/users/closure/weekly').
        success(function(response){
            console.log(response.data.length);
            var xAxis = {
                type: 'datetime'
            };
            var series =  [{
                name: 'Ratio',
                data: []
            }];
            
            var c=[];
            for(var m=0;m<response.data.length;m++){
                c[m]=new Date(response.data[m].date);
                series[0].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data[m].ratio]);
            }
            var title = {
                text: 'Weekly Users Closure'   
            };
            var subtitle = {
                text: 'Source: Base_Address/users/closure/weekly'
            };
            var yAxis = {
                title: {
                    text: 'Ratio ( Buisness/Paid )'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]};   
            var colors=['#004D75'];
//            var tooltip = {
//                valueSuffix: '$'
//            };

//        var legend = {
//            layout: 'vertical',
//            align: 'right',
//            verticalAlign: 'middle',
//            borderWidth: 0
//        };
            var json = {};
            json.title = title;
            json.subtitle = subtitle;
            json.xAxis = xAxis;
            json.yAxis = yAxis;
            json.colors=colors;
//            json.tooltip = tooltip;
            //   json.legend = legend;
            json.series = series;
            $timeout(function(){
                $scope.loading=false;
                $scope.loading3=false;
                $(document).ready(function() {
                    $('#container7').highcharts(json);
                });
            },2000);
        });
    };
    
     $scope.getmonthlyclosure=function(){
        $scope.loading3 = true;
        $scope.dailyclosure=false;
        $scope.weekclosure=false;
        $scope.monthclosure=true;
         //    ===========================monthly closure users===================
         $http.get('http://23.236.54.158:3000/users/closure/monthly').
         success(function(response){
             console.log(response.data.length);
             var xAxis = {
                 type: 'datetime'
             };
             var series =  [{
                 name: 'Ratio',
                 data: []
             }];
             var c=[];
             for(var m=0;m<response.data.length;m++){
                 c[m]=new Date(response.data[m].date);
                 series[0].data.push([Date.UTC(c[m].getFullYear(),c[m].getMonth(),c[m].getDate()),response.data[m].ratio]);
             }
             var title = {
                 text: 'Monthly Users Closure'   
             };
             var subtitle = {
                 text: 'Source: Base_Address/users/closure/monthly'
             };
             var yAxis = {
                 title: {
                     text: 'Ratio ( Buisness/Paid )'
                 },
                 plotLines: [{
                     value: 0,
                     width: 1,
                     color: '#808080'
                 }]};   
             var colors=['#004D75'];
//             var tooltip = {
//                 valueSuffix: '$'
//             };
             //        var legend = {
             //            layout: 'vertical',
             //            align: 'right',
             //            verticalAlign: 'middle',
             //            borderWidth: 0
             //        };
             
             var json = {};
             json.title = title;
             json.subtitle = subtitle;
             json.xAxis = xAxis;
             json.yAxis = yAxis;
             json.colors=colors;
//             json.tooltip = tooltip;
             //   json.legend = legend;
             json.series = series;
             $timeout(function(){
                 $scope.loading=false;
                 $scope.loading3=false;
                 $(document).ready(function() {
                     $('#container8').highcharts(json);
                 });
             },2000);
         });
     };
});